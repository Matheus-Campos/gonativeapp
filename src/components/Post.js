import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFF',
    padding: 20,
    marginTop: 20,
    marginHorizontal: 20,
    borderRadius: 5,
  },
  title: {
    color: '#333333',
  },
  author: {
    color: '#999999',
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  description: {
    color: '#666666',
  },
});

const Post = ({ title, author, description }) => (
  <View style={styles.container}>
    <Text style={styles.title}>{title}</Text>
    <Text style={styles.author}>{author}</Text>
    <Text style={styles.description}>{description}</Text>
  </View>
);

Post.propTypes = {
  title: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
};

export default Post;
