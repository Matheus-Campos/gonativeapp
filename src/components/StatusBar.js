import React from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';

const styles = StyleSheet.create({
  statusBar: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FFF',
  },
  statusBarText: {
    fontWeight: 'bold',
    fontSize: 24,
    color: '#333333',
  },
});

const StatusBar = ({ title }) => (
  <View style={styles.statusBar}>
    <Text style={styles.statusBarText}>{title}</Text>
  </View>
);

StatusBar.propTypes = {
  title: PropTypes.string.isRequired,
};

export default StatusBar;
