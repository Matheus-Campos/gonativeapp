import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  ScrollView,
} from 'react-native';

import 'config/ReactotronConfig';
import 'config/DevToolsConfig';
import Post from 'components/Post';
import StatusBar from 'components/StatusBar';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EE7777',
  },
});


export default class App extends Component {
  state = {
    posts: [
      {
        id: 0,
        title: 'Aprendendo React Native',
        author: 'Diego Fernandes',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin vulputate lacus a suscipit. Sed eget augue imperdiet, maximus justo ac, sodales ex. Curabitur tellus libero, luctus in tempor vel, luctus quis sem. Pellentesque ornare elit libero, in vulputate mi molestie et.',
      },
      {
        id: 1,
        title: 'Aprendendo React Native',
        author: 'Diego Fernandes',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin vulputate lacus a suscipit. Sed eget augue imperdiet, maximus justo ac, sodales ex. Curabitur tellus libero, luctus in tempor vel, luctus quis sem. Pellentesque ornare elit libero, in vulputate mi molestie et.',
      },
      {
        id: 2,
        title: 'Aprendendo React Native',
        author: 'Diego Fernandes',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin vulputate lacus a suscipit. Sed eget augue imperdiet, maximus justo ac, sodales ex. Curabitur tellus libero, luctus in tempor vel, luctus quis sem. Pellentesque ornare elit libero, in vulputate mi molestie et.',
      },
      {
        id: 3,
        title: 'Aprendendo React Native',
        author: 'Diego Fernandes',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin vulputate lacus a suscipit. Sed eget augue imperdiet, maximus justo ac, sodales ex. Curabitur tellus libero, luctus in tempor vel, luctus quis sem. Pellentesque ornare elit libero, in vulputate mi molestie et.',
      },
      {
        id: 4,
        title: 'Aprendendo React Native',
        author: 'Diego Fernandes',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin vulputate lacus a suscipit. Sed eget augue imperdiet, maximus justo ac, sodales ex. Curabitur tellus libero, luctus in tempor vel, luctus quis sem. Pellentesque ornare elit libero, in vulputate mi molestie et.',
      },
      {
        id: 5,
        title: 'Aprendendo React Native',
        author: 'Diego Fernandes',
        description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean sollicitudin vulputate lacus a suscipit. Sed eget augue imperdiet, maximus justo ac, sodales ex. Curabitur tellus libero, luctus in tempor vel, luctus quis sem. Pellentesque ornare elit libero, in vulputate mi molestie et.',
      },
    ],
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar title="GoNative App" />
        <ScrollView>
          {this.state.posts.map(post => (
            <Post key={post.id} title={post.title} author={post.author} description={post.description} />
          ))}
        </ScrollView>
      </View>
    );
  }
}
